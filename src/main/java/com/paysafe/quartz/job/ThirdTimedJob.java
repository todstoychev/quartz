package com.paysafe.quartz.job;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DisallowConcurrentExecution
public class ThirdTimedJob implements Job {

  private static final Logger logger = LoggerFactory.getLogger(ThirdTimedJob.class);

  @Override
  public void execute(JobExecutionContext context) throws JobExecutionException {
    logger.info("=====> ThirdTimedJob started.");

    try {
      Thread.sleep(20000);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      e.printStackTrace();

      throw new JobExecutionException(e.getMessage());
    }

    logger.info("=====> ThirdTimedJob finished.");
  }
}
