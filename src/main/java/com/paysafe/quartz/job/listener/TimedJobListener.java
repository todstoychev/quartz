package com.paysafe.quartz.job.listener;

import com.paysafe.quartz.job.FirstTimedJob;
import com.paysafe.quartz.job.SecondTimedJob;
import com.paysafe.quartz.job.ThirdTimedJob;
import java.time.Instant;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.JobListener;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

public class TimedJobListener implements JobListener {

  private final String name;

  private boolean shouldExecuteSecondJob;
  private boolean shouldExecuteThirdJob;

  public TimedJobListener(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void jobToBeExecuted(JobExecutionContext context) {
    shouldExecuteSecondJob = shouldExecuteSecondJob(context);
    shouldExecuteThirdJob = shouldExecuteThirdJob(context);
  }

  @Override
  public void jobExecutionVetoed(JobExecutionContext context) {
    // Skipped
  }

  @Override
  public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
    if (context.getJobInstance() instanceof FirstTimedJob && shouldExecuteSecondJob) {
      shouldExecuteSecondJob = false;
      Scheduler scheduler = context.getScheduler();
      JobKey jobKey = new JobKey(SecondTimedJob.class.toString());

      try {
        scheduler.triggerJob(jobKey);
      } catch (SchedulerException schedulerException) {
        schedulerException.printStackTrace();
      }
    }

    if (context.getJobInstance() instanceof SecondTimedJob && shouldExecuteThirdJob) {
      shouldExecuteThirdJob = false;
      Scheduler scheduler = context.getScheduler();
      JobKey jobKey = new JobKey(ThirdTimedJob.class.toString());

      try {
        scheduler.triggerJob(jobKey);
      } catch (SchedulerException schedulerException) {
        schedulerException.printStackTrace();
      }
    }
  }

  private boolean shouldExecuteSecondJob(JobExecutionContext context) {
    long time = context.getScheduledFireTime().getTime();
    Instant instant = Instant.ofEpochMilli(time);
    String seconds = instant.toString().substring(17, 19);

    return seconds.equals("14") || seconds.equals("29") || seconds.equals("44") || seconds.equals("59");
  }

  private boolean shouldExecuteThirdJob(JobExecutionContext context) {
    long time = context.getScheduledFireTime().getTime();
    Instant instant = Instant.ofEpochMilli(time);
    String seconds = instant.toString().substring(17, 19);

    return shouldExecuteThirdJob || seconds.equals("59");
  }
}
