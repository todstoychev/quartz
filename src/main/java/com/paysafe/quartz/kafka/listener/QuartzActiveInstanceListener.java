package com.paysafe.quartz.kafka.listener;

import com.paysafe.quartz.config.QuartzSchedulerConfig;
import com.paysafe.quartz.entity.Config;
import com.paysafe.quartz.kafka.event.QuartzActiveInstanceEvent;
import com.paysafe.quartz.repository.ConfigRepository;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

@Component
public class QuartzActiveInstanceListener {

  private static final Logger logger = LoggerFactory.getLogger(QuartzActiveInstanceListener.class);

  private final ConfigRepository configRepository;
  private final SchedulerFactoryBean schedulerFactory;

  @Value("${server.port}")
  private String port;

  public QuartzActiveInstanceListener(
      ConfigRepository configRepository,
      @Qualifier(QuartzSchedulerConfig.NODE_SPECIFIC_SCHEDULER_FACTORY) SchedulerFactoryBean schedulerFactory) {

    this.configRepository = configRepository;
    this.schedulerFactory = schedulerFactory;
  }

  @KafkaListener(topics = {"${kafka.quartz.topic}"},
      properties = {"spring.json.value.default.type=com.paysafe.quartz.kafka.event.QuartzActiveInstanceEvent"})
  public void listenAsObject(@Payload QuartzActiveInstanceEvent payload) throws UnknownHostException {
    logger.info("=====> Quartz scheduler active instance change event received.");

    Config config = configRepository.findByProperty("activeInstanceId");

    if (config == null) {
      config = new Config();
      config.setProperty("activeInstanceId");
    }

    config.setValue(payload.getActiveInstanceId());
    configRepository.save(config);

    String currentInstanceId = InetAddress.getLocalHost().getHostName() + ":" + port;

    if (!currentInstanceId.equals(config.getValue())) {
      schedulerFactory.stop();
      logger.info("=====> Node {} has been stopped.", currentInstanceId);
    } else {
      schedulerFactory.start();
      logger.info("=====> Node {} has been started.", currentInstanceId);
    }
  }
}
