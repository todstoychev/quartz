package com.paysafe.quartz.kafka.event;

public class QuartzActiveInstanceEvent {

  private String activeInstanceId;

  public String getActiveInstanceId() {
    return activeInstanceId;
  }

  public void setActiveInstanceId(String activeInstanceId) {
    this.activeInstanceId = activeInstanceId;
  }
}
