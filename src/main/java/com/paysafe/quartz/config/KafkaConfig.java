package com.paysafe.quartz.config;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.support.serializer.JsonDeserializer;

@Configuration
public class KafkaConfig {

  @Value("${server.port}")
  private String port;

  private final KafkaProperties kafkaProperties;

  public KafkaConfig(KafkaProperties kafkaProperties) {
    this.kafkaProperties = kafkaProperties;
  }

  @Bean
  public Map<String, Object> consumerConfig() throws UnknownHostException {
    String groupId = InetAddress.getLocalHost().getHostName() + ":" + port;

    Map<String, Object> props = new HashMap<>(kafkaProperties.buildConsumerProperties());
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
    props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
    props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
    props.put(ConsumerConfig.HEARTBEAT_INTERVAL_MS_CONFIG, 500);
    props.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_uncommitted");
    props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, 10000);
    props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, 30000);

    // Removing the type from headers, else we need a contract between both sides.
    props.put(JsonDeserializer.REMOVE_TYPE_INFO_HEADERS, true);

    // Trust all packages because this is general config.
    props.put(JsonDeserializer.TRUSTED_PACKAGES, "*");

    return props;
  }

  @Bean
  public ConsumerFactory<String, Object> consumerFactory() throws UnknownHostException {
    return new DefaultKafkaConsumerFactory<>(consumerConfig());
  }

  @Bean
  public ConcurrentKafkaListenerContainerFactory<String, Object> kafkaListenerContainerFactory()
      throws UnknownHostException {
    ConcurrentKafkaListenerContainerFactory<String, Object> factory = new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(consumerFactory());
    factory.setConcurrency(1);
    factory.setAutoStartup(true);
    factory.getContainerProperties().setPollTimeout(1000);
    factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.RECORD);

    return factory;
  }
}
