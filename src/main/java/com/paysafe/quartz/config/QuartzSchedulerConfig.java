package com.paysafe.quartz.config;

import com.paysafe.quartz.entity.Config;
import com.paysafe.quartz.repository.ConfigRepository;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import javax.sql.DataSource;
import org.quartz.simpl.SimpleJobFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

@Configuration
public class QuartzSchedulerConfig {

  public static final String DEFAULT_SCHEDULER_FACTORY = "defaultSchedulerFactory";
  public static final String NODE_SPECIFIC_SCHEDULER_FACTORY = "nodeSpecificSchedulerFactory";
  public static final String TIMED_JOB_SCHEDULER_FACTORY = "timedJobSchedulerFactory";

  private static final Logger logger = LoggerFactory.getLogger(QuartzSchedulerConfig.class);
  private static final String INSTANCE_NAME_PROPERTY = "org.quartz.scheduler.instanceName";

  private final ConfigRepository configRepository;

  @Value("${server.port}")
  private String port;

  private final String currentInstanceId;

  public QuartzSchedulerConfig(ConfigRepository configRepository) throws UnknownHostException {
    this.currentInstanceId = InetAddress.getLocalHost().getHostName() + ":" + this.port;
    this.configRepository = configRepository;
  }

  @Bean(name = NODE_SPECIFIC_SCHEDULER_FACTORY)
  public SchedulerFactoryBean nodeSpecificSchedulerFactory(DataSource dataSource) throws IOException {
    Config config = configRepository.findByProperty("activeInstanceId");

    // Sets activeInstanceId to current one in the database.
    if (config == null) {
      config = new Config();
      config.setProperty("activeInstanceId");
      config.setValue(currentInstanceId);
      configRepository.save(config);
    }

    String activeInstanceId = config.getValue();

    SchedulerFactoryBean factory = new SchedulerFactoryBean();
    factory.setBeanName(NODE_SPECIFIC_SCHEDULER_FACTORY);
    factory.setDataSource(dataSource);
    factory.setJobFactory(new SimpleJobFactory());

    quartzProperties().setProperty(INSTANCE_NAME_PROPERTY, "nodeSpecificScheduler");

    factory.setQuartzProperties(quartzProperties());

    if (!activeInstanceId.equals(currentInstanceId)) {
      factory.setAutoStartup(false);
      logger.info("=====> Node {} did not autostart.", currentInstanceId);
    } else {
      logger.info("=====> Node {} has autostart.", currentInstanceId);
    }

    factory.setAutoStartup(false);

    return factory;
  }

  @Bean(name = DEFAULT_SCHEDULER_FACTORY)
  public SchedulerFactoryBean defaultSchedulerFactory(DataSource dataSource) throws IOException {
    SchedulerFactoryBean factory = new SchedulerFactoryBean();
    factory.setBeanName(DEFAULT_SCHEDULER_FACTORY);
    factory.setDataSource(dataSource);
    factory.setJobFactory(new SimpleJobFactory());

    quartzProperties().setProperty(INSTANCE_NAME_PROPERTY, "defaultScheduler");

    factory.setQuartzProperties(quartzProperties());
    factory.setAutoStartup(false);

    return factory;
  }

  @Bean(name = TIMED_JOB_SCHEDULER_FACTORY)
  public SchedulerFactoryBean timedJobSchedulerFactory(DataSource dataSource) throws IOException {
    SchedulerFactoryBean factory = new SchedulerFactoryBean();
    factory.setBeanName(TIMED_JOB_SCHEDULER_FACTORY);
    factory.setDataSource(dataSource);
    factory.setJobFactory(new SimpleJobFactory());

    quartzProperties().setProperty(INSTANCE_NAME_PROPERTY, "timedJobScheduler");

    factory.setQuartzProperties(quartzProperties());
    factory.setAutoStartup(false);

    return factory;
  }

  @Bean
  public Properties quartzProperties() throws IOException {
    PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
    propertiesFactoryBean.setLocation(new ClassPathResource("/quartz.properties"));
    propertiesFactoryBean.afterPropertiesSet();

    return propertiesFactoryBean.getObject();
  }
}
