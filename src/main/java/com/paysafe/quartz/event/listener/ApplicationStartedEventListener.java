package com.paysafe.quartz.event.listener;

import com.paysafe.quartz.scheduler.SimpleChainedJobsScheduler;
import com.paysafe.quartz.scheduler.NodeSpecificJobScheduler;
import com.paysafe.quartz.scheduler.TimedJobScheduler;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStartedEventListener implements ApplicationListener<ApplicationStartedEvent> {

  private final NodeSpecificJobScheduler nodeSpecificJobScheduler;
  private final SimpleChainedJobsScheduler simpleChainedJobsScheduler;
  private final TimedJobScheduler timedJobScheduler;

  public ApplicationStartedEventListener(NodeSpecificJobScheduler nodeSpecificJobScheduler,
      SimpleChainedJobsScheduler simpleChainedJobsScheduler, TimedJobScheduler timedJobScheduler) {

    this.nodeSpecificJobScheduler = nodeSpecificJobScheduler;
    this.simpleChainedJobsScheduler = simpleChainedJobsScheduler;
    this.timedJobScheduler = timedJobScheduler;
  }

  @Override
  public void onApplicationEvent(ApplicationStartedEvent event) {
    nodeSpecificJobScheduler.schedule();
    simpleChainedJobsScheduler.schedule();
    timedJobScheduler.schedule();
  }
}
