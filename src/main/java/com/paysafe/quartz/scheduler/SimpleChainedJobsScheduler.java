package com.paysafe.quartz.scheduler;

import static org.quartz.CronScheduleBuilder.cronSchedule;

import com.paysafe.quartz.config.QuartzSchedulerConfig;
import com.paysafe.quartz.job.FirstJob;
import com.paysafe.quartz.job.SecondJob;
import com.paysafe.quartz.job.ThirdJob;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.listeners.JobChainingJobListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

@Service
public class SimpleChainedJobsScheduler implements SchedulerInterface {

  private static final Logger logger = LoggerFactory.getLogger(SimpleChainedJobsScheduler.class);

  private final Scheduler scheduler;

  public SimpleChainedJobsScheduler(
      @Qualifier(QuartzSchedulerConfig.DEFAULT_SCHEDULER_FACTORY) SchedulerFactoryBean schedulerFactoryBean) {

    this.scheduler = schedulerFactoryBean.getScheduler();
  }

  @Override
  public void schedule() {
    JobChainingJobListener jobListener = new JobChainingJobListener("SimpleChainedJobListener");

    // Jobs
    JobKey firstJobKey = new JobKey("FirstJob");

    TriggerKey jobTriggerKey = new TriggerKey("FirstJobTrigger");

    Trigger trigger = TriggerBuilder.newTrigger()
        .withIdentity(jobTriggerKey)
        .withSchedule(cronSchedule("0 * * ? * * *").withMisfireHandlingInstructionDoNothing())
        .withDescription("Chained jobs trigger.")
        .forJob(firstJobKey)
        .build();

    try {
      if (scheduler.checkExists(firstJobKey)) {
        if (scheduler.checkExists(jobTriggerKey)) {
          scheduler.rescheduleJob(jobTriggerKey, trigger);
        } else {
          scheduler.scheduleJob(trigger);
        }
      } else {
        JobDetail firstJobDetail = JobBuilder.newJob(FirstJob.class).withIdentity(firstJobKey).storeDurably().build();

        scheduler.scheduleJob(firstJobDetail, trigger);
      }

      JobKey secondJobKey = new JobKey("SecondJob");
      JobDetail secondJobDetail = JobBuilder.newJob(SecondJob.class).withIdentity(secondJobKey).storeDurably().build();

      JobKey thirdJobKey = new JobKey("ThirdJob");
      JobDetail thirdJobDetail = JobBuilder.newJob(ThirdJob.class).withIdentity(thirdJobKey).storeDurably().build();

      scheduler.addJob(secondJobDetail, true);
      scheduler.addJob(thirdJobDetail, true);

      jobListener.addJobChainLink(firstJobKey, secondJobKey);
      jobListener.addJobChainLink(secondJobKey, thirdJobKey);

      scheduler.getListenerManager().addJobListener(jobListener);
    } catch (SchedulerException exception) {
      logger.error("Failed to schedule chained jobs!", exception);
    }
  }
}
