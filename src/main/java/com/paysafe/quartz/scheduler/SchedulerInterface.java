package com.paysafe.quartz.scheduler;

public interface SchedulerInterface {

  void schedule();
}
