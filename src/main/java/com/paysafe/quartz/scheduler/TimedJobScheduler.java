package com.paysafe.quartz.scheduler;

import static org.quartz.CronScheduleBuilder.cronSchedule;

import com.paysafe.quartz.config.QuartzSchedulerConfig;
import com.paysafe.quartz.job.FirstTimedJob;
import com.paysafe.quartz.job.SecondTimedJob;
import com.paysafe.quartz.job.ThirdTimedJob;
import com.paysafe.quartz.job.listener.TimedJobListener;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

@Service
public class TimedJobScheduler implements SchedulerInterface {

  private static final Logger logger = LoggerFactory.getLogger(TimedJobScheduler.class);

  private final Scheduler scheduler;

  public TimedJobScheduler(
      @Qualifier(QuartzSchedulerConfig.TIMED_JOB_SCHEDULER_FACTORY) SchedulerFactoryBean schedulerFactoryBean) {

    this.scheduler = schedulerFactoryBean.getScheduler();
  }

  public void schedule() {
    try {
      JobKey firstJobKey = new JobKey(FirstTimedJob.class.toString());
      TriggerKey triggerKey = new TriggerKey("FirstTimedJobTrigger");

      Trigger trigger = TriggerBuilder.newTrigger()
          .withIdentity(triggerKey)
          .withSchedule(cronSchedule("* * * ? * * *").withMisfireHandlingInstructionDoNothing())
          .forJob(firstJobKey)
          .withDescription("First timed job trigger.").build();

      if (scheduler.checkExists(firstJobKey)) {
        if (scheduler.checkExists(triggerKey)) {
          scheduler.rescheduleJob(scheduler.getTriggersOfJob(firstJobKey).get(0).getKey(), trigger);
        } else {
          scheduler.scheduleJob(trigger);
        }
      } else {
        JobDetail jobDetail = JobBuilder.newJob(FirstTimedJob.class).withIdentity(firstJobKey).storeDurably().build();

        scheduler.scheduleJob(jobDetail, trigger);
      }

      JobKey secondJobKey = new JobKey(SecondTimedJob.class.toString());
      JobDetail secondJobDetail = JobBuilder.newJob(SecondTimedJob.class)
          .withIdentity(secondJobKey)
          .withDescription("Second timed job.")
          .storeDurably()
          .build();

      JobKey thirdJobKey = new JobKey(ThirdTimedJob.class.toString());
      JobDetail thirdJobDetail = JobBuilder.newJob(ThirdTimedJob.class)
          .withIdentity(thirdJobKey)
          .withDescription("Third timed job.")
          .storeDurably()
          .build();

      scheduler.addJob(secondJobDetail, true);
      scheduler.addJob(thirdJobDetail, true);
      scheduler.getListenerManager().addJobListener(new TimedJobListener("TimedJobListener"));

    } catch (SchedulerException schedulerException) {
      logger.error("Failed to schedule timed jobs!", schedulerException);
    }
  }
}
