package com.paysafe.quartz.scheduler;

import static org.quartz.CronScheduleBuilder.cronSchedule;

import com.paysafe.quartz.config.QuartzSchedulerConfig;
import com.paysafe.quartz.job.NodeSpecificJob;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

@Service
public class NodeSpecificJobScheduler implements SchedulerInterface {

  private static final Logger logger = LoggerFactory.getLogger(NodeSpecificJobScheduler.class);

  private final Scheduler scheduler;

  public NodeSpecificJobScheduler(
      @Qualifier(QuartzSchedulerConfig.NODE_SPECIFIC_SCHEDULER_FACTORY) SchedulerFactoryBean schedulerFactoryBean) {

    this.scheduler = schedulerFactoryBean.getScheduler();
  }

  public void schedule() {
    try {
      JobKey jobKey = new JobKey("NodeSpecificJob");
      TriggerKey triggerKey = new TriggerKey("NodeSpecificTrigger");

      Trigger trigger = TriggerBuilder.newTrigger()
          .withIdentity(triggerKey)
          .withSchedule(cronSchedule("0 * * ? * * *").withMisfireHandlingInstructionDoNothing())
          .forJob(jobKey)
          .withDescription("Node specific job trigger.").build();

      if (scheduler.checkExists(jobKey)) {
        if (scheduler.checkExists(triggerKey)) {
          scheduler.rescheduleJob(scheduler.getTriggersOfJob(jobKey).get(0).getKey(), trigger);
        } else {
          scheduler.scheduleJob(trigger);
        }
      } else {
        JobDetail jobDetail = JobBuilder.newJob(NodeSpecificJob.class).withIdentity(jobKey).storeDurably().build();

        scheduler.scheduleJob(jobDetail, trigger);
      }
    } catch (SchedulerException schedulerException) {
      logger.error("Failed to start Node specific job!", schedulerException);
    }
  }
}
